import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
          light: {
            success: '#4B9C7B',
            danger: '#C85B5B'
          }
        },
      },
});
